from django.db import models


class Task(models.Model):
    STATUSES = (
        ('open', 'open'),
        ('progres', 'progres'),
        ('test', 'test'),
	    ('done', 'done'),
    )
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True)
    status = models.CharField(max_length=100, choices=STATUSES)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name
