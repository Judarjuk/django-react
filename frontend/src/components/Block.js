import React from 'react'
import Task from './Task'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { getTasks } from '../actions/tasks'

class Block extends React.Component {

    static propTypes = {
        tasks: PropTypes.array.isRequired,
        getTasks: PropTypes.func.isRequired,
    }
    
    componentDidMount(){
        this.props.getTasks();
    }

    render(){
        return (
            <div className="taskblock">
                <p className="bloc-title">{this.props.children}</p>
                {this.props.tasks.map((task, i) => {
                    if(this.props.children == task.status){
                        return <Task key={i} {...task}>{task.name}</Task>
                    }
                })}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    tasks: state.tasksReducer.tasks
})

export default connect(mapStateToProps, {getTasks})(Block)