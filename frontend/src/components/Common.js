import React from 'react'
import Block from './Block'
import Form from './Form'

class Common extends React.Component {
    render(){
        const statuses = ['open', 'progres', 'test', 'done'];
        return (
            <div>
                <Form />
                <div className="tasks">
                    {statuses.map((state, i) => {
                        return <Block key={i}>{state}</Block>
                    })}
                </div>
            </div>
        );
    }
}

export default Common