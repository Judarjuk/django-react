import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from '../store';
import Common from './Common';

class App extends React.Component {
    render(){
        return (
            <Provider store={store}>
                <Fragment>
                    <Common />
                </Fragment>
            </Provider>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));