import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { addTask } from '../actions/tasks'

class Form extends React.Component {
    state = {
        name: '',
        description: '',
        formState: false,
        errorMessage: '',
        error: false,
    }

    static propTypes = {
        addTask: PropTypes.func.isRequired,
        errors: PropTypes.object.isRequired,
    }

    onChange = e => this.setState({ [e.target.name]: e.target.value });

    onSubmit = e => {
        e.preventDefault();
        const { name, description } = this.state;
        const status = 'open'
        const task = { name, description, status };
        this.props.addTask(task);
        if(name != ''){    
            this.setState({
                name: "",
                description: "",
                formState: false,
                errorMessage: '',
                error: false
            });
        }else{
            this.setState({
                error: true
            });
        }
    };

    openModal = () =>{
        this.setState({
            formState: true,
            errorMessage: '',
            error: false
        });
    }

    clouseModal = e => {
        e.preventDefault();
        this.setState({
            formState: false,
            errorMessage: '',
            error: false
        });
    }

    componentDidUpdate(prevProps){
        const {errors} = this.props;
        console.log(this.props);
        if(errors !== prevProps.error){
            if(errors.name){
                if(this.state.errorMessage !== `Name: ${errors.name[0]}`){
                    this.setState({errorMessage: `Name: ${errors.name[0]}`});
                }
            }
        }
    }

    render(){
        const { name, description } = this.state;
        return (
            <div className="add-form">
                <form onSubmit={this.onSubmit} className={ this.state.formState ? '' : 'hidden' }>
                    <p className={this.state.error?'errors':'hidden errors'}>{this.state.errorMessage}</p>
                    <p><a onClick={this.clouseModal} className="clouse-window">Clouse</a></p>
                    <p><label>Name: </label><br/><input type="text" name="name" onChange={this.onChange} value={name} /></p>
                    <p><label>Description: </label><br/><textarea rows="10" cols="40" name="description" onChange={this.onChange} value={description} ></textarea></p>
                    <p><button className="add-bottom" type="submit">Add</button></p>
                </form>
                <button onClick={this.openModal} className={ this.state.formState ? 'hidden add-bottom' : 'add-bottom' }>add task</button>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    errors: state.errorsReducer.errors,
});

export default connect(mapStateToProps, { addTask })(Form)