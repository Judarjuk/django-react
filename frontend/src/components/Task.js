import React from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { updateTask, deleteTask } from '../actions/tasks'

class Task extends React.Component {
    state = {
        id: this.props.id,
        name: this.props.name,
        description: this.props.description,
        status: this.props.status,
        modalState: false,
        errorMessage: '',
        error: false,
    };

    static propTypes = {
        updateTask: PropTypes.func.isRequired,
        deleteTask: PropTypes.func.isRequired,
    }

    onChange = e => this.setState({ [e.target.name]: e.target.value });

    showModal = e => {
        e.preventDefault();
        this.setState({
            modalState: true,
            errorMessage: '',
            error: false
        });
    }

    clouseModal = e => {
        e.preventDefault();
        this.setState({
            modalState: false,
            errorMessage: '',
            error: false
        });
    }

    onSubmit = e => {
        e.preventDefault();
        const { id, name, description, status } = this.state;
        const task = { id, name, description, status };
        this.props.updateTask(task);
        if(name != ''){
            this.setState({
                modalState: false,
                errorMessage: '',
                error: false
            });
        }else{
            this.setState({
                error: true
            });
        }
    };

    deleteTask = e => {
        e.preventDefault();
        this.props.deleteTask(this.props.id);
        this.setState({
            modalState: false,
        });
    }

    componentDidUpdate(prevProps){
        const {errors} = this.props;
        if(errors !== prevProps.error){
            if(errors.name){
                if(this.state.errorMessage !== `Name: ${errors.name[0]}`){
                    this.setState({errorMessage: `Name: ${errors.name[0]}`});
                }
            }
        }
    }

    render(){
        const { name, description, status } = this.state;
        const statuses = ['open', 'progres', 'test', 'done'];
        return (
            <div className="task-item">
                <a onClick={this.showModal}>{this.props.children}</a>
                <div className={ this.state.modalState ? 'modal-wrapper' : 'hidden modal-wrapper' }>
                    <form onSubmit={this.onSubmit}>
                        <p className={this.state.error?'errors':'hidden errors'}>{this.state.errorMessage}</p>
                        <p><a onClick={this.clouseModal} className="clouse-window">Clouse</a></p>
                        <p><label>Name: </label><br/><input type="text" name="name" value={name} onChange={this.onChange} /></p>
                        <p><label>Description: </label><br/><textarea rows="10" name="description" cols="40" value={description} onChange={this.onChange}></textarea></p>
                        <p><label>Status: </label><br/>
                            <select name="status" value={status} onChange={this.onChange}>
                                {statuses.map((curent_state, i) => {
                                    return <option key={i}>{curent_state}</option>
                                })}
                            </select>
                        </p>
                        <p><button type="submit" className="add-bottom">submit</button>&nbsp; &nbsp; &nbsp; <button onClick={this.deleteTask} className="add-bottom" type="button">remove</button></p>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    errors: state.errorsReducer.errors,
});

export default connect(mapStateToProps, { updateTask, deleteTask })(Task)