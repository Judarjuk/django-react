import axios from 'axios'
import { GET_TASKS, UPDATE_TASK, ADD_TASK, DELETE_TASK, GET_ERRORS } from './types';

export const getTasks = () => dispatch => {
    var csrftoken = getCookie('csrftoken');
    axios.get('/api/tasks/', {
        headers: { "X-CSRFToken": csrftoken }
    }).then(res => {
        dispatch({
            type: GET_TASKS,
            payload: res.data,
        });
    }).catch(err => console.log(err));
}

export const updateTask = (task) => dispatch => {
    var csrftoken = getCookie('csrftoken');
    axios.put(`/api/tasks/${task.id}/`, task, {
        headers: { "X-CSRFToken": csrftoken }
    }).then(res => {
        dispatch({
            type: UPDATE_TASK,
            payload: res.data,
        });
    }).catch(err => {
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data,
        });
        console.log(err)
    });
}

export const deleteTask = (id) => dispatch => {
    var csrftoken = getCookie('csrftoken');
    axios.delete(`/api/tasks/${id}/`, {
        headers: { "X-CSRFToken": csrftoken }
    }).then(res => {
        dispatch({
            type: DELETE_TASK,
            payload: id,
        });
    }).catch(err => console.log(err));
}

export const addTask = (task) => dispatch => {
    var csrftoken = getCookie('csrftoken');
    axios.post('/api/tasks/', task, {
        headers: { "X-CSRFToken": csrftoken }
    }).then(res => {
        dispatch({
            type: ADD_TASK,
            payload: res.data,
        });
    }).catch((err) => {
        dispatch({
            type: GET_ERRORS,
            payload: err.response.data,
        });
        console.log(err)
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}