import { combineReducers } from 'redux';
import errors from './errors';
import tasks from './tasks';

export default combineReducers({
    tasksReducer: tasks,
    errorsReducer: errors,
});