import { GET_TASKS, UPDATE_TASK, ADD_TASK, DELETE_TASK } from '../actions/types';

const initialState = {
    tasks: [],
}

export default function(state = initialState, action){
    switch(action.type){
        case GET_TASKS:
            return {
                ...state,
                tasks: action.payload
            };
        case UPDATE_TASK:
            return {
                ...state,
                tasks: state.tasks.map(item => {
                    return item.id === action.payload.id ? action.payload : item;
                })
            };
        case DELETE_TASK:
            return {
                ...state,
                tasks: state.tasks.filter(task => task.id !== action.payload)
            };
        case ADD_TASK:
            return {
                ...state,
                tasks: [...state.tasks, action.payload]
            };
        default:
            return state;
    }
}