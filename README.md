# Tasks Manager

> Full stack Django/React/Redux app.

```bash
# Install dependencies
cd frontend/
npm install

# Serve API on localhost:8000
python manage.py runserver

# Run webpack (from root)
cd frontend/
npm run dev

# Build for production
cd frontend/
npm run build
```